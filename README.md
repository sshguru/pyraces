# Testing installation (dev server):

1. Install Python
2. Intall flask: 
```
python -m pip install flask
```
3. Clone this repo
4. Start Flask dev:
```
python ~/pyraces/pyraces.py
```
5. Test on http://127.0.0.1:8080
6. CSV export available on http://127.0.0.1:8080/csv


# "Prod" installation:

1. Download pyraces.py file
2. Run the following:
```
chmod +x ~/pyraces.py
```
3. Start script:
```
~/pyraces.py
```
4. Note: script assumes that no Apache etc configuration exists on the system and expects the OS to be Ubuntu/Debian etc
5. Note2: you can use the content of the script to figure out how to configure this on a pre-existing Apache box